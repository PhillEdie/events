Pré-requisitos para execução:
- Instação do docker e docker-cli

Para executar a aplicação, execute os seguintes comandos:
- docker-compose build
- docker-compose up

Para testar os serviços requisitados no enunciado,consuma os seguintes endpoints:
- POST http://localhost:5750/events/ (para registro de um novo evento)
- GET http://localhost:5750/events/:term/autocomplete (para autocompletar um termo)
- GET http://localhost:5750/events/timeline (para obter a timeline de eventos)