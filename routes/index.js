const express = require('express');
const router = express.Router();
const events = require('../app/eventCtrl');

router.post('/', function (req, res, next) {
  const {
    event,
    timestamp
  } = req.body;

  events.create(event, timestamp)
  .then(event => res.status(201).json(event))
  .catch(error => res.json({
    error: error
  }));
});

router.get('/:term/autocomplete', function (req, res, next) {
  const term = req.params.term;

  events.autocomplete(term)
    .then(event => {
      if (event) {
        res.status(200).json(event);
      } else {
        res.status(204).end();
      }
    })
    .catch(error => res.json({
      error: error
    }));
});

router.get('/timeline', function (req, res, next) {
  const term = req.params.term;

  events.timeline(term)
    .then(event => res.status(200).json(event))
    .catch(error => res.json({
      error: error
    }));
});

module.exports = router;
