const model = require('../models/index');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const axios = require('axios');

class EventRep {
    constructor(EventRepository) {
        this.eventRepository = EventRepository;
    }

    async findBy(event) {
        return model.Event.findOne({
            where: {
                event: {
                    [Op.like]: `${event}%`
                }
            }
        })
    }

    async create(event, timestamp) {
        const response = await model.Event.findOrCreate({
            where: {
                event: event
            },
            defaults: {
                event: event,
                timestamp: timestamp
            }
        });

        return response[0];
    }

    async getEvents() {
        return axios.get('https://storage.googleapis.com/dito-questions/events.json')
            .then(response => {
                response = response.data;
                return response.events;
            });
    }
}

module.exports = EventRep;