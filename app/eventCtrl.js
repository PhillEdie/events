const EventBs = require('./eventBs');
const EventRepository = require('./eventRep');

async function create(event, timestamp) {
    const repository = new EventRepository();
    const business = new EventBs(repository);
    
    return business.create(event, timestamp);
}

async function autocomplete(term) {
    const repository = new EventRepository();
    const business = new EventBs(repository);

    return business.autocomplete(term);
}

async function timeline(term) {
    const repository = new EventRepository();
    const business = new EventBs(repository);

    return business.getTimeline();
}

module.exports = {
    create,
    autocomplete,
    timeline
};