class EventsBs {
    constructor(EventRepository) {
        this.eventRepository = EventRepository;
    }

    async autocomplete(term) {
        return this.eventRepository
            .findBy(term);
    }

    async create(event, timestamp) {
        return await this.eventRepository
            .create(event, timestamp);
    }

    async getTimeline() {
        const events = await this.eventRepository
            .getEvents();

        const extratData = function (customData) {
            let formattedData = {
                product: {}
            };
            customData.forEach(data => {
                if (data.key == 'transaction_id') {
                    formattedData.transaciontId = data.value;
                }
                if (data.key == 'product_name') {
                    formattedData.product.name = data.value
                }
                if (data.key == 'product_price') {
                    formattedData.product.price = data.value
                }
                if (data.key == 'store_name') {
                    formattedData.store_name = data.value
                }
            });
            return formattedData;
        }

        let products = {};
        let timeline = [];
        events.forEach(event => {
            let checkout = {}
            const data = extratData(event.custom_data);
            if (!products[data.transaciontId] && data.product.name) {
                products[data.transaciontId] = [{
                    name: data.product.name,
                    price: data.product.price
                }];
            } else if (data.product.name) {
                products[data.transaciontId].push({
                    name: data.product.name,
                    price: data.product.price
                });
            }
            if (event.event == 'comprou') {
                checkout.timestamp = event.timestamp;
                checkout.revenue = event.revenue;
                checkout.transaction_id = data.transaciontId;
                checkout.store_name = data.store_name;
                timeline.push(checkout);
            }
        });

        timeline.forEach(checkout => {
            checkout.products = products[checkout.transaction_id]
        });

        timeline.sort(function (a, b) {
            if (new Date(a.timestamp) < new Date(b.timestamp)) {
                return 1;
            }
            if (new Date(a.timestamp) > new Date(b.timestamp)) {
                return -1;
            }
            return 0;
        });

        return {
            timeline: timeline
        };
    }
}

module.exports = EventsBs;