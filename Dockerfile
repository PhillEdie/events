FROM node:10.15.1

RUN mkdir -p /usr/src/events
RUN chmod -R 777 /usr/src/events

WORKDIR /usr/src/events

COPY package*.json /usr/src/events/
COPY . /usr/src/events

RUN npm install
RUN npm install -g sequelize sequelize-cli
RUN npm install -g mysql2

RUN git clone https://github.com/vishnubob/wait-for-it.git

